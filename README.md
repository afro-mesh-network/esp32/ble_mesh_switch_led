| Supported Targets | ESP32 |
| ----------------- | ----- |

# ble mesh switch led demo
## Introduction  
This demo implements ble mesh node basic features.Based on this demo, node can be scaned and proved by provisioner, reply get/set message to provisioner.
[ **_Detailed is here_** ][1]

Demo steps:  
1.Build the ble mesh switch led demo with sdkconfig.default  
2.register node and set oob info, load model to init ble mesh node

	esp32> bmreg
	E (13562) ble\_mesh\_console: enter ble\_mesh\_register\_cb
	
	I (13562) ble\_mesh\_console: Bm:Reg,OK
	esp32> bmoob -o 0 -x 0
	I (19442) ble\_mesh\_console: OOB:Load,OK
	
	esp32> bminit -m 0x1000
	I (24762) ble\_mesh\_console: Bm:Init,OK
	
	esp32> bmnbearer -b 0x3 -e 1
	I (62542) ble\_mesh\_console: Node:EnBearer,OK
3.enable bearer, so that it can be scaned and provisioned by provisioner


[1]:	https://medium.com/@ma510/bluetooth-mesh-network-1184a557571e
